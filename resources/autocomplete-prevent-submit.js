/**
 * Created by Henke_Christian on 21.09.2016.
 */
/*(function($) {

    Drupal.behaviors.ACChangeEnterBehavior = {
        attach: function(context, settings) {
            $('input.form-autocomplete', context).once('ac-change-enter-behavior', function() {
                $(this).keypress(function(e) {
                    var ac = $('#autocomplete');
                    if (e.keyCode == 13 && typeof ac[0] != 'undefined') {
                        e.preventDefault();
                        var fauxEvent = $.Event('mousedown'); // Create an event to trigger
                        $('li.selected', ac).trigger(fauxEvent);
                    }
                });
            });
        }
    }

})(jQuery);*/
// This Function prevents a submit when "Enter" is clicked
(function($) {

    Drupal.behaviors.DisableInputEnter = {
        attach: function(context, settings) {
            $('input', context).once('disable-input-enter', function() {
                $(this).keypress(function(e) {
                    if (e.keyCode == 13) {
                        e.preventDefault();
                    }
                });
            });
        }
    }

})(jQuery);