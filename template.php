<?php

/**
 * @file
 * template.php
 */


function sfb_alm_dashboard_preprocess_html(&$vars) {
  $vars['classes_array'][] = 'sidebar-collapse';
  $vars['classes_array'][] = 'sidebar-mini';
  $vars['classes_array'][] = 'hold-transition';
  $vars['classes_array'][] = 'skin-black';
}

function sfb_alm_dashboard_preprocess_page(&$variables) {

  // get user variables
  global $user;

  //TODO: prüfen ob user is anonymous

  $userc = UsersRepository::findByUid($user->uid);
  $user_fullname = $userc->getFullname(true);
  $variables['user_fullname'] = $user_fullname;

  $user_fullname2 = $userc->getFullname();
  $variables['user_fullname2'] = $user_fullname2;

  $manager_permission = user_access(SFB_ALM_PERMISSION_MANAGE);
  $variables['manager_permission'] = $manager_permission;

  $search_request_form = drupal_get_form('sfb_alm_template_form_search');
  $search_request = drupal_render($search_request_form);
  $variables['search_request'] = $search_request;

  //$form = drupal_get_form('sfb_alm_request_view_form');
  //$search_box = drupal_render($form);
  //$variables['search_box'] = $search_box;
}