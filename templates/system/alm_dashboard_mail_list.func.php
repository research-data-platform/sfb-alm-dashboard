<?php

function sfb_alm_dashboard_alm_dashboard_mail_list($variables) {

  /*
   *  array(
   *   'inbox_new_mails' => integer,
   *   'inbox_active' => boolean,
   *   'inbox_link' => string,
   *   'sent_active' => boolean,
   *   'archived_active' => boolean
   *   'trash_new_mails' => integer,
   *   'trash_active' => boolean,
   *   'emails' => array(
   *     //array with entries for alm_dashboard_maillist_row
   *   )
   *
   */

  // render emails table
  // if there are no emails, then print a message
  $mail_rows = '';
  if(count($variables['emails']) == 0) {
    $mail_rows .= '<tr><td>'.t('Your inbox is empty').'</td></tr>';
  } else {

    foreach($variables['emails'] as $email) {
      $mail_rows .= '
        <tr'.(!$email['is_read'] ? ' style="font-weight: bold;"' : '').'>
          <td><input type="checkbox"></td>
          <td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
          <td class="mailbox-name"><a href="'.$email['link'].'">'.$email['sender'].'</a></td>
          <td class="mailbox-subject">'.$email['subject'].'</td>
          <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>
          <td class="mailbox-date">'.$email['date'].'</td>
        </tr>';
    }

  }

  $output = '';

  $output .= '
    <div class="row">
        <div class="col-lg-3">
            <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li'.($variables['inbox_active'] ? ' class="active"' : '').'><a href="'.$variables['inbox_link'].'"><i class="fa fa-inbox"></i> Inbox '.($variables['inbox_new_mails'] > 0 ? '<span class="label label-primary pull-right">'.$variables['inbox_new_mails'].'</span>' : '').'</a></li>
                <li'.($variables['sent_active'] ? ' class="active"' : '').'><a href="'.$variables['sent_link'].'"><i class="fa fa-envelope-o"></i> Sent</a></li>
                <li'.($variables['archived_active'] ? ' class="active"' : '').'><a href="'.$variables['archived_link'].'"><i class="fa fa-file-text-o"></i> Archived</a></li>
                <li'.($variables['trash_active'] ? ' class="active"' : '').'><a href="'.$variables['trash_link'].'"><i class="fa fa-trash-o"></i> Trash</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <div class="col-lg-9">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Inbox</h3>

              <div class="box-tools pull-right">
                <div class="has-feedback">
                  <input type="text" class="form-control input-sm" placeholder="Search Mail">
                  <span class="glyphicon glyphicon-search form-control-feedback"></span>
                </div>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                  1-50/200
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                  '.$mail_rows.'
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
                </button>
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                <div class="pull-right">
                  1-50/200
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
            </div>
          </div>
          <!-- /. box -->

      </div>
    </div>
  ';

  return $output;
}