<?php
/**
 * @file
 * Stub file for bootstrap_table().
 */

/**
 * Returns HTML for a table.
 *
 * @param array $variables
 *   An associative array containing:
 *   - header: An array containing the table headers. Each element of the array
 *     can be either a localized string or an associative array with the
 *     following keys:
 *     - "data": The localized title of the table column.
 *     - "field": The database field represented in the table column (required
 *       if user is to be able to sort on this column).
 *     - "sort": A default sort order for this column ("asc" or "desc"). Only
 *       one column should be given a default sort order because table sorting
 *       only applies to one column at a time.
 *     - Any HTML attributes, such as "colspan", to apply to the column header
 *       cell.
 *   - rows: An array of table rows. Every row is an array of cells, or an
 *     associative array with the following keys:
 *     - "data": an array of cells
 *     - Any HTML attributes, such as "class", to apply to the table row.
 *     - "no_striping": a boolean indicating that the row should receive no
 *       'even / odd' styling. Defaults to FALSE.
 *     Each cell can be either a string or an associative array with the
 *     following keys:
 *     - "data": The string to display in the table cell.
 *     - "header": Indicates this cell is a header.
 *     - Any HTML attributes, such as "colspan", to apply to the table cell.
 *     Here's an example for $rows:
 * @code
 *     $rows = array(
 *       // Simple row
 *       array(
 *         'Cell 1', 'Cell 2', 'Cell 3'
 *       ),
 *       // Row with attributes on the row and some of its cells.
 *       array(
 *         'data' => array('Cell 1', array('data' => 'Cell 2', 'colspan' => 2)), 'class' => array('funky')
 *       )
 *     );
 * @endcode
 *   - footer: An array containing the table footer. Each element of the array
 *     can be either a localized string or an associative array with the
 *     following keys:
 *     - "data": The localized title of the table column.
 *     - "field": The database field represented in the table column (required
 *       if user is to be able to sort on this column).
 *     - "sort": A default sort order for this column ("asc" or "desc"). Only
 *       one column should be given a default sort order because table sorting
 *       only applies to one column at a time.
 *     - Any HTML attributes, such as "colspan", to apply to the column footer
 *       cell.
 *   - attributes: An array of HTML attributes to apply to the table tag.
 *   - caption: A localized string to use for the <caption> tag.
 *   - colgroups: An array of column groups. Each element of the array can be
 *     either:
 *     - An array of columns, each of which is an associative array of HTML
 *       attributes applied to the COL element.
 *     - An array of attributes applied to the COLGROUP element, which must
 *       include a "data" attribute. To add attributes to COL elements, set the
 *       "data" attribute with an array of columns, each of which is an
 *       associative array of HTML attributes.
 *     Here's an example for $colgroup:
 * @code
 *     $colgroup = array(
 *       // COLGROUP with one COL element.
 *       array(
 *         array(
 *           'class' => array('funky'), // Attribute for the COL element.
 *         ),
 *       ),
 *       // Colgroup with attributes and inner COL elements.
 *       array(
 *         'data' => array(
 *           array(
 *             'class' => array('funky'), // Attribute for the COL element.
 *           ),
 *         ),
 *         'class' => array('jazzy'), // Attribute for the COLGROUP element.
 *       ),
 *     );
 * @endcode
 *     These optional tags are used to group and set properties on columns
 *     within a table. For example, one may easily group three columns and
 *     apply same background style to all.
 *   - sticky: Use a "sticky" table header.
 *   - empty: The message to display in an extra row if table does not have any
 *     rows.
 *
 * @return string
 *   The constructed HTML.
 *
 * @see theme_table()
 *
 * @ingroup theme_functions
 */
function sfb_alm_dashboard_alm_dashboard_box($variables) {

  /**
   *
   *  SYNTAX:
   *  
   *  - Header
   *
   *  $variables['header'] =
   *    array(
   *      'title' => STRING,
   *      'tools' => array(
   *        collapse => BOOLEAN
   *        remove => BOOLEAN
   *      ),
   *    );
   *
   *  - Body
   *  
   *  $variables['body'] = array(
   *    'data' => STRING,
   *  );
   *  
   *  $variables['footer'] = array(
   *    'data' => STRING,
   *  );
   *
   */

  $header = $variables['header'];
  $title = $variables['header']['title'];
  $body = $variables['body']['data'];
  $footer = $variables['footer']['data'];
  
  $body_classes = '';
  if(isset($variables['body']['class'])) {
    foreach ($variables['body']['class'] as $class) {
      $body_classes .= ' '.$class;
    }
  
  }

  $output = '<div class="box">';

  if(!empty($title)) {
    $output .= '<div class="box-header with-border">
                    <h3 class="box-title">'.$title.'</h3>
                    <!--<div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                    </div>-->
                </div>';
  }

  $output .= '  <div class="box-body '.$body_classes.'">
                    '.$body.'
                </div>
                <!-- /.box-body -->';

  if(!empty($footer))
    $output .= '<div class="box-footer">'.$footer.'</div><!-- /.box-footer-->';

  $output .= '</div>';

  return $output.'<hr />';
}
