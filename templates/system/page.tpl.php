<?php
/**
 * @file
 */
?>

<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo url('alm', array('absolute' => true)); ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><img src="<?php print base_path() . drupal_get_path('theme', 'sfb_alm_dashboard') .'/img/alm_logo.png'; ?>" alt="ALM"/></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><img src="<?php print base_path() . drupal_get_path('theme', 'sfb_alm_dashboard') .'/img/alm_logo_full.png'; ?>" alt="Advanced Light Microscopy"/></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li>
                        <?php print $search_request; ?>
                    </li>
                    
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>
                            <span class="hidden-xs"><?php print $user_fullname; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <i class="fa fa-user"></i>
                                <p>
                                    <?php print $user_fullname2; ?>
                                    <small>
                                    <?php
                                    if ($manager_permission)
                                        print 'ALM Manager';
                                    else
                                        print 'Researcher'
                                    ?>
                                    </small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?php print base_path() ?>user" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?php print base_path() ?>user/logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li><a href="<?php echo url('alm', array('absolute' => true)); ?>"><i class="fa fa-dashboard"></i> <span>ALM Main page</span></a></li>
                <li><a href="<?php echo url('alm/request', array('absolute' => true)); ?>"><i class="fa fa-files-o"></i> <span>Requests</span></a></li>
                <li><a href="<?php echo url('alm/request/new', array('absolute' => true)); ?>"><i class="fa fa-asterisk"></i> <span>New Request</span></a></li>
                <?php
                    if ($manager_permission):
                ?>
                <li class="header">SERVICE 02</li>
                <li><a href="<?php echo url('alm/management/request/', array('absolute' => true)); ?>"><i class="fa fa-reorder"></i> <span>Manage requests</span></a></li>
                <?php
                    endif;
                ?>
                <!--<li><a href="<?php echo url('alm/documentation', array('absolute' => true)); ?>"><i class="fa fa-book"></i> <span>Documentation</span></a></li>-->
                <li><a href="<?php echo url('/', array('absolute' => true)); ?>"><i class="fa fa-reply"></i> <span>Back to RDP</span></a></li>
                
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <?php if (!empty($title)): ?>
            <h1><?php print $title; ?></h1>
            <small><?php //print sfb_echo_template_get_subtitle(); ?></small>
            <?php endif; ?>
            <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php print $messages; ?>
            <?php print render($page['content']); ?>
            <br />
            <br />
        </section>
        <!-- /.content -->
        
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        Advanced Light Microscopy (ALM) - SFB 1002 Service 02
    </footer>

</div>
<!-- ./wrapper -->

<!-- SlimScroll -->
<script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'sfb_alm_dashboard') .'/plugins/slimScroll/jquery.slimscroll.min.js'; ?>"></script>
<!-- DataTables -->
<script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'sfb_alm_dashboard') .'/plugins/datatables/jquery.dataTables.min.js'; ?>"></script>
<script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'sfb_alm_dashboard') .'/plugins/datatables/dataTables.bootstrap.min.js'; ?>"></script>

<!-- FastClick -->
<script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'sfb_alm_dashboard') .'/plugins/fastclick/fastclick.js'; ?>"></script>

<!-- AdminLTE App -->
<script type="text/javascript" src="<?php print base_path() . drupal_get_path('theme', 'sfb_alm_dashboard') .'/dist/js/app.min.js'; ?>"></script>

<!-- page script -->
<script>
    jQuery(function () {
        jQuery("#example1").DataTable();
        jQuery('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>