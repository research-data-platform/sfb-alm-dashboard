<?php

function sfb_alm_dashboard_alm_dashboard_mail_view($variables) {
  $output = '';

  $output_reply = '';
  if(count($variables['reply']) > 0) {

    $output_reply = '<ul class="timeline">';

    foreach($variables['reply'] as $reply) {
      $output_reply .= '
        <!-- timeline time label -->
        <li class="time-label">
            <span class="bg-red">
                '.$reply['message_to'].'
            </span>
        </li>';

      $output_reply .= '
        <!-- timeline item -->
        <li>
            <!-- timeline icon -->
            <i class="fa fa-envelope bg-blue"></i>
            <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> '.$reply['message_date'].'</span>

                <h3 class="timeline-header">'.$reply['message_title'].'</h3>

                <div class="timeline-body">
                    '.$reply['message_content'].'
                </div>

                <div class="timeline-footer">
                </div>
            </div>
        </li>
        <!-- END timeline item -->';
    }

    $output_reply .= '</ul>';

  }

  $output .= '
    <div class="row">
        <div class="col-lg-3">
            <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li'.($variables['inbox_active'] ? ' class="active"' : '').'><a href="'.$variables['inbox_link'].'"><i class="fa fa-inbox"></i> Inbox</a></li>
                <li'.($variables['sent_active'] ? ' class="active"' : '').'><a href="'.$variables['sent_link'].'><a href="'.$variables['sent_link'].'"><i class="fa fa-envelope-o"></i> Sent</a></li>
                <li'.($variables['archived_active'] ? ' class="active"' : '').'><a href="'.$variables['archived_link'].'><a href="'.$variables['archived_link'].'"><i class="fa fa-file-text-o"></i> Archived</a></li>
                <li'.($variables['trash_active'] ? ' class="active"' : '').'><a href="'.$variables['trash_link'].'><a href="'.$variables['trash_link'].'"><i class="fa fa-trash-o"></i> Trash</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Read Mail</h3>

              <div class="box-tools pull-right">
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
                <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3>'.$variables['message_title'].'</h3>
                <h5>From: '.$variables['message_from'].'<span class="mailbox-read-time pull-right">'.$variables['message_date'].'</span></h5>
              </div>
              <!-- /.mailbox-read-info -->
              <div class="mailbox-controls with-border text-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete">
                    <i class="fa fa-trash-o"></i></button>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Reply">
                    <i class="fa fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Forward">
                    <i class="fa fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" title="Print">
                  <i class="fa fa-print"></i></button>
              </div>
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                '.$variables['message_content'].'
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">
                <button type="button" class="btn btn-default"><i class="fa fa-reply"></i> Reply</button>
                <button type="button" class="btn btn-default"><i class="fa fa-share"></i> Forward</button>
              </div>
              <button type="button" class="btn btn-default"><i class="fa fa-trash-o"></i> Delete</button>
              <button type="button" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->




    '.$output_reply.'


        </div>
        <!-- /.col -->
    </div>
  ';

  return $output;
}